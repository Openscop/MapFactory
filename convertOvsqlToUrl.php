<?php
require_once('begin.inc.php');
if(!is_connect()){
    header('Location:index.php');
    exit;
}
$layer = new Layer;
$layer->set('id',$_POST['idLayer']);
$tabTag = $layer->loadTag();
$url = 'http://overpass-api.de/api/interpreter?data=';
$url .= urlencode('[out:json];');
$url .= urlencode($_POST['ovsql']);
$tags = "";
foreach ($tabTag as $tag){
    $tags .= '"'.$tag['id_feature'].'":'.$tag['tag'].",";
}
$tags = substr($tags,0,-1);
echo '{"url":"'.$url.'","tags":{'.$tags.'}}';