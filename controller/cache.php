<?php
/**
 * Created by David
 * Date: 02/06/2015
 * Time: 22:27
 */

session_start();

require_once(PATH_ROOT."/model/Cache.class.php");


$state = isset($_GET['set_state']) ? $_GET['set_state'] : null;
$get_state = isset($_GET['get_state']) ? $_GET['get_state'] : null;

$cache = new Cache();

if ($state)
{
	$cache->set_state($state);
}
else if ($get_state)
{
	$jsonFile = new JsonFile();
	$jsonFile->addContent($cache->get_state());
	$jsonFile->render();
}
