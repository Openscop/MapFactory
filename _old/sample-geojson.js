
var test = {
    "type": "FeatureCollection",
    "generator": "overpass-turbo",
    "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL.",
    "timestamp": "2015-05-12T11:17:03Z",
    "features": [
        {
            "type": "Feature",
            "id": "way/249870638",
            "properties": {
                "@id": "way/249870638",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            4.3864937,
                            45.4468372
                        ],
                        [
                            4.3866658,
                            45.4462854
                        ],
                        [
                            4.3867374,
                            45.4464094
                        ],
                        [
                            4.3866023,
                            45.4468494
                        ],
                        [
                            4.3865838,
                            45.4468648
                        ],
                        [
                            4.3865549,
                            45.446872
                        ],
                        [
                            4.3865168,
                            45.4468656
                        ],
                        [
                            4.3865006,
                            45.4468526
                        ],
                        [
                            4.3864937,
                            45.4468372
                        ]
                    ]
                ]
            }
        },
        {
            "type": "Feature",
            "id": "way/338940542",
            "properties": {
                "@id": "way/338940542",
                "barrier": "fence",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            4.3823784,
                            45.449973
                        ],
                        [
                            4.3824161,
                            45.449909
                        ],
                        [
                            4.3826673,
                            45.4499508
                        ],
                        [
                            4.3826594,
                            45.4499654
                        ],
                        [
                            4.3823784,
                            45.449973
                        ]
                    ]
                ]
            }
        },
        {
            "type": "Feature",
            "id": "way/339942396",
            "properties": {
                "@id": "way/339942396",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            4.3989129,
                            45.4164425
                        ],
                        [
                            4.3989961,
                            45.4164538
                        ],
                        [
                            4.398945,
                            45.416531
                        ],
                        [
                            4.3989048,
                            45.4165234
                        ],
                        [
                            4.3989129,
                            45.4164425
                        ]
                    ]
                ]
            }
        },
        {
            "type": "Feature",
            "id": "way/340069067",
            "properties": {
                "@id": "way/340069067",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Polygon",
                "coordinates": [
                    [
                        [
                            4.3927959,
                            45.4347265
                        ],
                        [
                            4.3928495,
                            45.4348074
                        ],
                        [
                            4.3927637,
                            45.4347886
                        ],
                        [
                            4.3927959,
                            45.4347265
                        ]
                    ]
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/1717671541",
            "properties": {
                "@id": "node/1717671541",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3795591,
                    45.4480461
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/1717671546",
            "properties": {
                "@id": "node/1717671546",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3803128,
                    45.4448641
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/2812650501",
            "properties": {
                "@id": "node/2812650501",
                "amenity": "potty_area",
                "dog": "designated",
                "name": "Espace canin"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3940823,
                    45.4262295
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3461285681",
            "properties": {
                "@id": "node/3461285681",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3985979,
                    45.4140606
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3461373548",
            "properties": {
                "@id": "node/3461373548",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3790224,
                    45.4415478
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3461381046",
            "properties": {
                "@id": "node/3461381046",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4009699,
                    45.4126628
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3470993225",
            "properties": {
                "@id": "node/3470993225",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3821483,
                    45.4516911
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471103455",
            "properties": {
                "@id": "node/3471103455",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3907114,
                    45.4420689
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471115648",
            "properties": {
                "@id": "node/3471115648",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3967403,
                    45.4468498
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471199410",
            "properties": {
                "@id": "node/3471199410",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3994526,
                    45.438717
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471230496",
            "properties": {
                "@id": "node/3471230496",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3906971,
                    45.4308066
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471242621",
            "properties": {
                "@id": "node/3471242621",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3960125,
                    45.4322175
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471271285",
            "properties": {
                "@id": "node/3471271285",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3980511,
                    45.4285416
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471329461",
            "properties": {
                "@id": "node/3471329461",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4066251,
                    45.4252821
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471330876",
            "properties": {
                "@id": "node/3471330876",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4078337,
                    45.4237489
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471340089",
            "properties": {
                "@id": "node/3471340089",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4098403,
                    45.4219274
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471352808",
            "properties": {
                "@id": "node/3471352808",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3987307,
                    45.4285691
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471397264",
            "properties": {
                "@id": "node/3471397264",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3996048,
                    45.4253123
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471401314",
            "properties": {
                "@id": "node/3471401314",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4015805,
                    45.4131293
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471405810",
            "properties": {
                "@id": "node/3471405810",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3983164,
                    45.4168327
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471406213",
            "properties": {
                "@id": "node/3471406213",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3974724,
                    45.4140561
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471406321",
            "properties": {
                "@id": "node/3471406321",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3962891,
                    45.4138318
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3471433479",
            "properties": {
                "@id": "node/3471433479",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4032361,
                    45.4484524
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3472737467",
            "properties": {
                "@id": "node/3472737467",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3945221,
                    45.4220336
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3472810323",
            "properties": {
                "@id": "node/3472810323",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4099807,
                    45.4260676
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3472832105",
            "properties": {
                "@id": "node/3472832105",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3982892,
                    45.4195259
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3472853384",
            "properties": {
                "@id": "node/3472853384",
                "leisure": "dog_park"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.4071945,
                    45.4115873
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3473014303",
            "properties": {
                "@id": "node/3473014303",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3897297,
                    45.4419702
                ]
            }
        },
        {
            "type": "Feature",
            "id": "node/3480733522",
            "properties": {
                "@id": "node/3480733522",
                "amenity": "potty_area",
                "dog": "designated"
            },
            "geometry": {
                "type": "Point",
                "coordinates": [
                    4.3823145,
                    45.451979
                ]
            }
        }
    ]
};


var testjson = {
    "version": 0.6,
    "generator": "Overpass API",
    "osm3s": {
        "timestamp_osm_base": "2015-05-12T19:54:02Z",
        "timestamp_areas_base": "2015-05-11T11:34:03Z",
        "copyright": "The data included in this document is from www.openstreetmap.org. The data is made available under ODbL."
    },
    "elements": [

        {
            "type": "node",
            "id": 1717671541,
            "lat": 45.4480461,
            "lon": 4.3795591,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 1717671546,
            "lat": 45.4448641,
            "lon": 4.3803128,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 2812650501,
            "lat": 45.4262295,
            "lon": 4.3940823,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated",
                "name": "Espace canin"
            }
        },
        {
            "type": "node",
            "id": 3461285681,
            "lat": 45.4140606,
            "lon": 4.3985979,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3461373548,
            "lat": 45.4415478,
            "lon": 4.3790224,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3461381046,
            "lat": 45.4126628,
            "lon": 4.4009699,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3470993225,
            "lat": 45.4516911,
            "lon": 4.3821483,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471103455,
            "lat": 45.4420689,
            "lon": 4.3907114,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471115648,
            "lat": 45.4468498,
            "lon": 4.3967403,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3471199410,
            "lat": 45.4387170,
            "lon": 4.3994526,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471230496,
            "lat": 45.4308066,
            "lon": 4.3906971,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471242621,
            "lat": 45.4322175,
            "lon": 4.3960125,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3471271285,
            "lat": 45.4285416,
            "lon": 4.3980511,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471329461,
            "lat": 45.4252821,
            "lon": 4.4066251,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471330876,
            "lat": 45.4237489,
            "lon": 4.4078337,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471340089,
            "lat": 45.4219274,
            "lon": 4.4098403,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471352808,
            "lat": 45.4285691,
            "lon": 4.3987307,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471397264,
            "lat": 45.4253123,
            "lon": 4.3996048,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471401314,
            "lat": 45.4131293,
            "lon": 4.4015805,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471405810,
            "lat": 45.4168327,
            "lon": 4.3983164,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471406213,
            "lat": 45.4140561,
            "lon": 4.3974724,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471406321,
            "lat": 45.4138318,
            "lon": 4.3962891,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471433479,
            "lat": 45.4484524,
            "lon": 4.4032361,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3472737467,
            "lat": 45.4220336,
            "lon": 4.3945221,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3472810323,
            "lat": 45.4260676,
            "lon": 4.4099807,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3472832105,
            "lat": 45.4195259,
            "lon": 4.3982892,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3472853384,
            "lat": 45.4115873,
            "lon": 4.4071945,
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "node",
            "id": 3473014303,
            "lat": 45.4419702,
            "lon": 4.3897297,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3480733522,
            "lat": 45.4519790,
            "lon": 4.3823145,
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "way",
            "id": 249870638,
            "nodes": [
                2564157684,
                2564157686,
                2564157688,
                2564157689,
                2564157687,
                2564157685,
                2564157683,
                2564157682,
                2564157684
            ],
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "way",
            "id": 338940542,
            "nodes": [
                3461278953,
                3461278950,
                3461278951,
                3461278952,
                3461278953
            ],
            "tags": {
                "barrier": "fence",
                "leisure": "dog_park"
            }
        },
        {
            "type": "way",
            "id": 339942396,
            "nodes": [
                3471404637,
                3471404638,
                3471404639,
                3471402424,
                3471404637
            ],
            "tags": {
                "leisure": "dog_park"
            }
        },
        {
            "type": "way",
            "id": 340069067,
            "nodes": [
                3472771361,
                3472771362,
                3472771363,
                3472771361
            ],
            "tags": {
                "amenity": "potty_area",
                "dog": "designated"
            }
        },
        {
            "type": "node",
            "id": 3471402424,
            "lat": 45.4165234,
            "lon": 4.3989048
        },
        {
            "type": "node",
            "id": 3471404637,
            "lat": 45.4164425,
            "lon": 4.3989129
        },
        {
            "type": "node",
            "id": 3471404638,
            "lat": 45.4164538,
            "lon": 4.3989961
        },
        {
            "type": "node",
            "id": 3471404639,
            "lat": 45.4165310,
            "lon": 4.3989450
        },
        {
            "type": "node",
            "id": 3472771361,
            "lat": 45.4347265,
            "lon": 4.3927959
        },
        {
            "type": "node",
            "id": 3472771362,
            "lat": 45.4348074,
            "lon": 4.3928495
        },
        {
            "type": "node",
            "id": 3472771363,
            "lat": 45.4347886,
            "lon": 4.3927637
        },
        {
            "type": "node",
            "id": 3461278950,
            "lat": 45.4499090,
            "lon": 4.3824161
        },
        {
            "type": "node",
            "id": 3461278951,
            "lat": 45.4499508,
            "lon": 4.3826673
        },
        {
            "type": "node",
            "id": 3461278952,
            "lat": 45.4499654,
            "lon": 4.3826594
        },
        {
            "type": "node",
            "id": 3461278953,
            "lat": 45.4499730,
            "lon": 4.3823784
        },
        {
            "type": "node",
            "id": 2564157682,
            "lat": 45.4462854,
            "lon": 4.3866658
        },
        {
            "type": "node",
            "id": 2564157683,
            "lat": 45.4464094,
            "lon": 4.3867374
        },
        {
            "type": "node",
            "id": 2564157684,
            "lat": 45.4468372,
            "lon": 4.3864937
        },
        {
            "type": "node",
            "id": 2564157685,
            "lat": 45.4468494,
            "lon": 4.3866023
        },
        {
            "type": "node",
            "id": 2564157686,
            "lat": 45.4468526,
            "lon": 4.3865006
        },
        {
            "type": "node",
            "id": 2564157687,
            "lat": 45.4468648,
            "lon": 4.3865838
        },
        {
            "type": "node",
            "id": 2564157688,
            "lat": 45.4468656,
            "lon": 4.3865168
        },
        {
            "type": "node",
            "id": 2564157689,
            "lat": 45.4468720,
            "lon": 4.3865549
        }

    ]
};
