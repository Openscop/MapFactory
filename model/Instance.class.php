<?php

/**
 * Created by David
 * Date: 02/06/2015
 * Time: 16:15
 */
class Instance
{

    private $db;
    private $layers = null;
    public $name = null;
    public $version = null;
    public $freebox1_content = null;
    public $custom_css = null;

    /**
     *
     */
    function __construct()
    {
        try {
            $this->db = new PDO("mysql:host=" . DB_INSTANCE_HOST . ";dbname=" . DB_INSTANCE_DATABASE, DB_INSTANCE_USERNAME, DB_INSTANCE_PASSWORD);
        } catch (PDOException $e) {
		echo 'error connection db ';
            if ($handle = fopen(PATH_LOG . "/bd.log", "a+")) {
                fwrite($handle, $e->getMessage());
                fclose($handle);
            }
            die();
        }
        $this->db->query("SET NAMES UTF8");
        $this->readConfig();
    }

    /**
     * Execute an UPDATE SQL request to change information about the language corresponding to the ID.
     */
    public function readConfig()
    {
        $req = $this->db->prepare("SELECT * FROM config WHERE 1;");
        $req->execute();
        $res = $req->fetchAll(PDO::FETCH_OBJ);
        foreach ($res as $value) {
            if ($value->name == "instance_name") {
                $this->name = $value->data;
            }
            if ($value->name == "version") {
                $this->version = $value->data;
            }
            if ($value->name == "freebox1_content") {
                $this->freebox1_content = $value->data;
            }
            if ($value->name == "custom_css") {
                $this->custom_css = $value->data;
            }
            if ($value->name == "about") {
                $this->about = $value->data;
            }
        }
    }

    /**
     * Execute an UPDATE SQL request to change information about the language corresponding to the ID.
     * @return array : all layers in array. Keys are layers 'id'
     */
    public function getAllLayers()
    {
        if (is_null($this->layers_json)) {
            $req = $this->db->prepare("SELECT * FROM layer WHERE 1;");
            $req->execute();
            $res = $req->fetchAll(PDO::FETCH_ASSOC);
            foreach ($res as $value) {
                $layers[$value['id']] = $value;
                unset($layers[$value['id']]['id']);
            }
            $this->layers = $layers;
        }
        return $this->layers;
    }
}
