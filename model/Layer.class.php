<?php

/**
 * Created by Alois
 * Date: 15/06/2016
 * Time: 13:38
 */

class Layer
{

	private $db;
	private $id = null;
	private $name = null;
	private $visible_by_default = null;
	private $osm_tag = null;
	private $ovsql = null;
	private $data_json = null;
	private $source = null;
        private $osm_wiki_link = null;
        private $technical_comment = null;
        private $color = null;
        
	
	/**
	 *
	 */
	function __construct($id = "")
	{
		try {
			$this->db = new PDO("mysql:host=" . DB_INSTANCE_HOST . ";dbname=" . DB_INSTANCE_DATABASE, DB_INSTANCE_USERNAME, DB_INSTANCE_PASSWORD);
		} catch (PDOException $e) {
		    if ($handle = fopen(PATH_LOG . "/bd.log", "a+")) {
		        fwrite($handle, $e->getMessage());
		        fclose($handle);
		    }
		    die();
		}
                if($id !== ""){
                    $cache = new Cache();
                    $this->db->query("SET NAMES UTF8");

                    $this->id = $id;

                    $req = $this->db->prepare("SELECT * FROM layer WHERE id = ".$id.";");
                    $req->execute();
                    $res = $req->fetch(PDO::FETCH_OBJ);

                    $this->name = $res->name;
                    $this->visible_by_default = $res->visible_by_default;
                    $this->osm_tag = $res->osm_tag;
                    $this->ovsql = $res->overpassql;
                    $this->source = $res->source; 
                    $this->osm_wiki_link = $res->osm_wiki_link;
                    $this->technical_comment = $res->technical_comment;
                    $this->color = $res->color;
                }
                $this->db->query("SET NAMES UTF8");
                
	}

	/**
	 * lancer une requête vers OSM pour récupérer les données de la couche
	 */
	
	private function osm_request() 
	{
		
		$url = 'http://overpass-api.de/api/interpreter?data=';
		$url .= urlencode('[out:json];');
		$url .= urlencode($this->ovsql);

		/* BOF error context time out trop court ????
		$ctx = stream_context_create(array('http'=>
		    array(
		        'timeout' => $global_settings['overspass_request_timeout'], // Seconds
		    )
		));
		// EOF error context time out trop court ???? */
		
		$this->data = file_get_contents($url, 0);
	}                               

	
	/**
	 * lancer une requête vers les layers ( ou couches) internes pour récupérer les données de la couche
	 */
	
	private function internal_request() 
	{
		$req = $this->db->prepare("SELECT geojson FROM layer_internal_data WHERE id_layer = ".$this->id);
		$req->execute();
		$res = $req->fetch(PDO::FETCH_OBJ);
		$this->data = $res->geojson;
	}
	
	/**
	 * lancer une requête vers le cache des layers ( ou couches) pour récupérer les données de la couche
	 */
	
	
	private function cache_request()
	{
		$table = array();
		$req = $this->db->prepare("SELECT id_layer, osm_data FROM cache WHERE id_layer ='".$this->id."';");
		$req->execute();
		$res = $req->fetch(PDO::FETCH_OBJ);
		$this->data = $res->osm_data;	
		$this->data = str_replace("\'", "'", $this->data);
	}
	
	/**
	 * @return string json les données du layer ( ou couche) via l'api overpassql, la table de cache ou la table des layers internes.
	 */
	
	public function get_data()
	{
		if ($_SESSION['work_with_cache'] == "true" && $this->source == "OSM")
		{
			$this->cache_request();	
		}
		else if ($this->source == "OSM")
		{
			$this->osm_request();
		}
		else if ($this->source == "internal")
		{
			$this->internal_request();
		}
		
		if ($this->data == null || empty($this->data) || !isset($this->data))
		{	
			$this->data = "error: no data found";
		}
	
		return $this->data;
	}
	
	/**
	 * @return int : id of the layer
	 */
	
	public function get_id()
	{
		return $this->id;
	}
        
        
        
        
        ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        
        public function set($field,$value){
            $this->$field = $value;    
        }
        
        public function getHtml($field){
            return htmlentities($this->$field);
            //return htmlspecialchars($this->$field,ENT_QUOTES,'UTF-8');
        }
        public function get($field){
            return $this->$field;
        }

                public function insert($id_taxonomy){
            // Role: ajout d'un nouveau layer
            // $id_taxonomy -> id de la table taxonomy_term à laquelle ratacher le layer
            // Pas de color dans l'objet ???
            // insertion base de donnée
            $req = $this->db->prepare("INSERT INTO `layer` SET `name` = :name,`overpassql` = :ovsql, `visible_by_default` = :visible_by_default, `osm_tag` = :osm_tag,`osm_wiki_link` = :wiki,`technical_comment` = :techComment, `color` = :color, `source` = :source");
            $req->execute([':name' => $this->name,':ovsql' => $this->ovsql,':visible_by_default' => $this->visible_by_default,':osm_tag' => $this->osm_tag,':wiki'=> $this->osm_wiki_link, ':techComment'=> $this->technical_comment, ':color'=> $this->color,':source' => $this->source]);
            $this->id = $this->db->lastInsertId();
            $req = $this->db->prepare("INSERT INTO `layer_taxonomy` SET `id_layer` = :layer, `id_taxonomy` = :taxonomy");
            $req->execute([':layer' => $this->id,':taxonomy' => $id_taxonomy]);
            if ($this->source === "internal"){
                $req = $this->db->prepare("INSERT INTO `layer_internal_data` SET `id_layer` = :id, `geojson` = :geojson");
                $req->execute([':id' => $this->id, ':geojson' => $this->data_json]);
            }    
        }

        public function update(){
            // Role: Maj d'une ligne de l bdd
            $req = $this->db->prepare("UPDATE `layer` SET `name` = :name,`overpassql` = :ovsql, `visible_by_default` = :visible_by_default, `osm_tag` = :osm_tag,`osm_wiki_link` = :wiki,`technical_comment` = :techComment, `color` = :color, `source` = :source WHERE `id` = :id");
            $req->execute([':id' => $this->id,':name' => $this->name, ':ovsql'=> $this->ovsql,':visible_by_default' => $this->visible_by_default,'osm_tag'=> $this->osm_tag,':wiki'=> $this->osm_wiki_link, ':techComment'=> $this->technical_comment, ':color'=> $this->color,':source'=> $this->source]);
            if($req->rowCount() > 0){
                return true;
            }
            return false;
        }
        
        public function delete(){
            // Role: Suprimer un layer de la bdd
            $param = [':id' => $this->id];
            $table = ['layer'=>'id','layer_taxonomy'=>'id_layer','layer_internal_data'=>'id_layer','layer_tag'=>'id_layer'];
            foreach ($table as $tableName => $idName){
                $req = $this->db->prepare("DELETE FROM `$tableName` WHERE `$idName` = :id");
                $req->execute($param);
            }           
        }
        
        public function SelectAll(){
            // Lister tout les layer et les classer par categories
            $req = $this->db->prepare("SELECT `layer`.*, `taxonomy_term`.`name` AS `taxonomy_name`, `taxonomy_term`.`id` AS `taxo_id` FROM `layer` LEFT JOIN `layer_taxonomy` ON `layer`.`id` = `layer_taxonomy`.`id_layer` LEFT JOIN `taxonomy_term` ON `layer_taxonomy`.`id_taxonomy`= `taxonomy_term`.`id`");
            $req->execute();
            $res = [];
            $lignes =$req->fetchAll(PDO::FETCH_ASSOC);
            foreach($lignes as $index=>$ligne){
                $layer = new Layer();
                $layer->db->query('KILL CONNECTION_ID()');
                $layer->db = null;
                $layer->id = $ligne['id'];
                $layer->name = $ligne['name'];
                $layer->ovsql = $ligne['overpassql'];
                $layer->visible_by_default = $ligne['visible_by_default'];
                $layer->osm_tag = $ligne['osm_tag'];
                
                
                $layer->source = $ligne['source'];
                $layer->osm_wiki_link = $ligne['osm_wiki_link'];
                $layer->technical_comment = $ligne['technical_comment'];
                $layer->color = $ligne['color'];
                if(!isset($res[$ligne['taxo_id']])){
                    $res[$ligne['taxo_id']] = ['titre'=>$ligne['taxonomy_name'],'layer'=>[]];                 
                }
                if (is_array($res[$ligne['taxo_id']]['layer'])){
                    array_push($res[$ligne['taxo_id']]['layer'], $layer);
                    

                }
            }
            
            
           return $res;
        }
        
        public function loadGeojson($id_layer){
            // Charger le geojson d'un layer
            $req = $this->db->prepare("SELECT `geojson` FROM `layer_internal_data` WHERE `id_layer` = :id");
            $req->execute([':id' => $id_layer]);
            $res = $req->fetch(PDO::FETCH_ASSOC);
            return $res['geojson'];          
        }
        
        public  function updateGeojson($geojson){
            // Mettre à jour le geojson du layer
            $req = $this->db->prepare("UPDATE `layer_internal_data` SET `geojson` = :geojson WHERE `id_layer` = :id");
            $req->execute([':geojson' => $geojson,':id' => $this->id]);
        }
        
        public function updateTag($tabTag){
            // Enregistrer les modification d'affichage  apporter aux popoup des données OSM
            if($tabTag !== []){
                $sqlDel = "DELETE FROM `layer_tag` WHERE ";
                $sqlInsert = "INSERT INTO `layer_tag` (`id_layer`,`id_feature`,`tag`) VALUES ";
                foreach($tabTag as $feature){
                    $idFeat = $feature['id_feature'];
                    $tag = '{';
                    foreach ($feature['tag'] as $key => $val){
                        $tag .= '"'.$key.'":{';
                        foreach($val as $key2 => $val2){
                            $tag .= '"'.$key2.'":'.$val2.',';
                        }
                        $tag = substr($tag,0,-1).'},';
                    }
                    $tag = substr($tag,0,-1).'}';
                    $sqlDel .= "(`id_layer` = $this->id AND `id_feature` = '$idFeat') OR ";
                    $sqlInsert .= "($this->id,'$idFeat','$tag'),";                 
                }
                $sqlDel = substr($sqlDel,0,-3);
                $sqlInsert = substr($sqlInsert, 0,-1);
            }
            $reqDel = $this->db->prepare($sqlDel);
            $reqDel->execute();
            $reqInsert = $this->db->prepare($sqlInsert);
            $reqInsert->execute();
        }
        
        public function loadTag(){
            // Chager les tag modifié d'un layer 
            $req = $this->db->prepare("SELECT `id_feature`,`tag` FROM `layer_tag` WHERE `id_layer` = :idLayer");
            $req->execute([':idLayer' => $this->id]);
            $res = $req->fetchALL(PDO::FETCH_ASSOC);
            return $res;
        }
        
        public function loadAllTag(){
            // Chager tous les tags modifié 
            $req = $this->db->prepare("SELECT `id_feature`,`tag` FROM `layer_tag`");
            $req->execute();
            $res = $req->fetchALL(PDO::FETCH_ASSOC);
            $tags = "";
            foreach ($res as $tag){
                $tags .= '"'.$tag['id_feature'].'":'.$tag['tag'].",";
            }
            $tags = substr($tags,0,-1);
            return '{"tags":{'.$tags.'}}';
        }
}
