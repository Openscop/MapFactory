<?php

class User{
    
    // Attributs
    private $db;
    private $id = "";
    private $name = "";
    private $password = "";
    
    // constructeur
    function __construct($id = 0) {
        try {
            $this->db = new PDO("mysql:host=" . DB_INSTANCE_HOST . ";dbname=" . DB_INSTANCE_DATABASE, DB_INSTANCE_USERNAME, DB_INSTANCE_PASSWORD);
        } catch (PDOException $e) {
            if ($handle = fopen(PATH_LOG . "/bd.log", "a+")) {
                fwrite($handle, $e->getMessage());
                fclose($handle);
            }
            die();
        }
        $this->db->query("SET NAMES UTF8");
        if($id !== 0){
            $this->loadFromId($id);
        }
    }
    
    // Méthodes
    public function get($field){
        return $this->$field;
    }
    
    public function set($field,$value){
        if(isset($this->$field)){
            $this->$field = $value;
        }
    }
    
    public function setFromTab($table){
        foreach ($table as $field => $value) {
            $this->set($field, $value);
        }
    }
    
    public function loadFromId($id){
        // Charger un utilisateur depuis son id
        $req = $this->db->prepare("SELECT * FROM `user` WHERE `id` = :id");
        $req->execute([':id' => $id]);
        if($res = $req->fetch(PDO::FETCH_ASSOC)){
            $this->id = $id;
            $this->name = $res['name'];
            $this->password = $res['password'];
            return true;
        }
        return false;
    }
    
    public function checkPassword($name,$password){
        // Charger l'utilisateur depuis son nom et vérifier la validité du mot de passe
        // Retour: 0 -> l'utilisateur n'existe pas
        //         1 -> l'utilisateur existe mais le mot de passe n'est pas bon
        //         2 -> l'utilisateur existe et le mot de passe est correct
        $req = $this->db->prepare("SELECT * FROM `user` WHERE `name` = :name");
        $req->execute([':name' => $name]);
        if($res = $req->fetch(PDO::FETCH_ASSOC)){
            $this->id = $res['id'];
            $this->name = $res['name'];
            $this->password = $res['password'];
            if(password_verify($password, $this->password)){
                return 2;
            }else{
                return 1;
            }
        }
        return 0;   
    }
    
    public function insert(){
        // Créer un nouvel utilisateur si aucun utilisateur du meme nom existe
        // Retour: 0 -> le nom de l'utilisateur existe déjà
        //         1 -> erreur lors de l'insertion dans la bdd
        //         2 -> l'utilisateur à été ajouté à la bdd
        $req = $this->db->prepare("SELECT * FROM `user` WHERE `name` = :name");
        $req->execute([':name' => $this->name]);
        if($req->fetch(PDO::FETCH_ASSOC)){
            return 0;
        }
        $this->password = password_hash($this->password, PASSWORD_DEFAULT);
        $req2 = $this->db->prepare("INSERT INTO `user` SET `name` = :name,`password` = :password");
        $req2->execute([':name' => $this->name,':password' => $this->password]);
        if($req2->rowCount() > 0){
            $this->id = $this->db->lastInsertId();
            return 2;
        }
        return 1;
    }
    
    public function liste(){
        // Lister tous les utilisateurs dans un tableau
        $req = $this->db->prepare("SELECT `id`,`name` FROM `user`");
        $req->execute();
        $res = [];
        while($table=$req->fetch(PDO::FETCH_ASSOC)){
            $res[$table['id']] = $table['name'];
        }
        return $res;
    }
    
    public function delete(){
        // Suprimer un utilisateur
        $req = $this->db->prepare("DELETE FROM `user` WHERE `id` = :id");
        $req->execute([':id' => $this->id]);
        if($req->rowCount() > 0){
            return true;
        }
        return false;
    }
}
