<?php
require_once('begin.inc.php');
if(!is_connect()){
    header('Location:index.php');
    exit;
}
if($_POST['mode'] === 'taxo'){
    $taxonomy = new Taxonomy;
    if($taxonomy->update($_POST['id'], $_POST['name'])){
        $list = ['id'=>$_POST['id'],'name'=>$_POST['name']];
        ob_start();
        include 'view/form/optionTaxonomy.php';
        $optionTaxonomy = ob_get_contents();
        ob_end_clean();
        $data = ['success',"#Taxo-".$_POST['id'],$_POST['name'],"#opt-".$_POST['id'],$optionTaxonomy,"#taxoTitle-".$_POST['id']];
    }else{
        $data = ['error'];
    }     
}elseif ($_POST['mode'] === 'layer') {
    $layer = new Layer;
    $layer->set('id', $_POST['id']);
    $layer->set('name', $_POST['name']);
    $layer->set('ovsql', $_POST['ovsql']);
    $layer->set('visible_by_default', $_POST['visible_by_default']);
    $layer->set('osm_tag', $_POST['osm_tag']);
    $layer->set('osm_wiki_link', $_POST['osm_wiki_link']);
    $layer->set('technical_comment', $_POST['technical_comment']);
    $layer->set('color', $_POST['color']);
    $layer->set('source', $_POST['source']);
    $layer->update();
    if($_POST['source'] === 'internal'){
        $layer->updateGeojson($_POST['geojson']);
    }else if($_POST['source'] === 'OSM'){
        $layer->updateTag($_POST['tag']);
    }
    ob_start();
    include 'view/form/listLayer.php';
    $html = ob_get_contents();
    ob_end_clean(); 
    $data = ['#layer-'.$_POST['id'],$html];
}
header('Content-Type: application/json');
echo json_encode($data);